//функция позволяет вводить в поле с типом number только цифры
//и ограничивает длину ввода по атрибуту maxlength

var inputNum = $('input[type=number]');
inputNum.keypress(function (e) {
    var inputLength = (e.target.value.length);
    var inputMaxLength = (e.target.getAttribute('maxlength'))

    if (inputLength > inputMaxLength) {
        e.preventDefault();
    }
    // e.which = 8  - backspace; e.which = 0 - delete
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        e.preventDefault();
    }
});
