var widthForQuery = 768;


function scrollCheck() {
    //проверка наличия скролла
    if (window.innerWidth > document.body.clientWidth) {
        //определение ширины скролла
        var div = document.createElement('div');

        div.style.overflowY = 'scroll';
        div.style.width = '50px';
        div.style.height = '50px';
        div.style.visibility = 'hidden';

        document.body.appendChild(div);
        var scrollWidth = div.offsetWidth - div.clientWidth;
        document.body.removeChild(div);

        widthForQuery = 768 - scrollWidth;

    }
    adaptivity();
}
//перемещение чекбокса
function adaptivity() {
    var w = $(window).width();
    var checkboxAddMsg = $('.checkbox-add_msg');

    if (w < widthForQuery) {
        $('.submit-button').before(checkboxAddMsg);
    }
    else {
        $('.checkbox-email').after(checkboxAddMsg);
    }
};

scrollCheck();

$(window).resize(function () {
    scrollCheck();
});