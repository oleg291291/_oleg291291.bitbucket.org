var cardNumInput = $('#sender-num')
cardNumInput.on("keyup", function () {
    var cardFirstNum = ($('#sender-num').val()[0]);
    switch (cardFirstNum) {
        case '4':
            $('.sender-card-img').css('background-image', 'url("resources/visa.svg")');
            break;
        case '5':
            $('.sender-card-img').css('background-image', 'url("resources/mastercard.svg")');
            break;
        case '6':
            $('.sender-card-img').css('background-image', 'url("resources/maestro.svg")');
            break;
        default:
            $('.sender-card-img').css('background-image', 'none');
    }
});
